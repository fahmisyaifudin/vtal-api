<?php

namespace App\Http\Middleware;
use App\Pasien;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;

use Closure;

class TokenCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('Token');

        if(!$token){
            return response()->json([
                'error' => 'Token is not found'
            ], 400);
        }

        try {
            $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
        } catch(ExpiredException $e) {
            return response()->json([
                'error' => 'Provided token is expired.'
            ], 400);
        } catch(Exception $e) {
            return response()->json([
                'error' => 'An error while decoding token.'
            ], 400);
        }
       
        $user = Pasien::where('id', $credentials->sub)->first();
        
        if ($user['id']) {
            $request->idFromToken = $user['id'];
            return $next($request);
        }else{
            return response()->json([
                'error' => 'Unidentified user.'
            ], 400);
        }
    }
}
