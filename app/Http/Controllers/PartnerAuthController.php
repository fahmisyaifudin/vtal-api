<?php

namespace App\Http\Controllers;
use App\Partner;
use Illuminate\Http\Request;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Hash;
use Firebase\JWT\ExpiredException;

class PartnerAuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function login(Request $request){
      
    }
    public function register(Request $request){
        $input = $request->all();
        $file = $request->file('image');
        $input['image'] = $input['image']->getClientOriginalName();
        $file->move('img/partner',$input['image']);
        $input['status'] = true;
        $input['password'] = app('hash')->make($input['password']);
        Partner::create($input);
        return response()->json($input);
    }

    //
}
