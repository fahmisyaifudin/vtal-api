<?php

namespace App\Http\Controllers;
use App\Pasien;
use Illuminate\Http\Request;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Hash;
use Firebase\JWT\ExpiredException;

class PasienAuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    protected function jwt($user) {
        $payload = [
            'iss' => "vtal-api", // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued. 
            'exp' => time() + 2592000*3 // Expiration time 3 Bulan
        ];
        // As you can see we are passing `JWT_SECRET` as the second parameter that will 
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));
    } 
    public function register(Request $request){
        $input = $request->all();
        $file = $request->file('image');
        $input['image'] = $input['image']->getClientOriginalName();
        $file->move('img/pasien',$input['image']);
        $input['status'] = true;
        $input['password'] = app('hash')->make($input['password']);
        Pasien::create($input);
        return response()->json($input);
    }
    public function login(Request $request){
        $input = $request->all();
        $user = Pasien::where('email', $input['email'])->first();
        if(!$user){
            return response()->json([
                'error' => 'Email tidak terdaftar'
            ], 400);
        }
        if ($input['password'] == $user['password']) {
            return response()->json([
                'message' => 'success',
                'token' => $this->jwt($user)
            ], 200);
        }else{
            return response()->json([
                'error' => 'Password tidak cocok'
            ], 400);
        }
    }
    public function show(Request $request){
        $data['pasien'] = Pasien::where('id', $request->idFromToken)->first();
        return response()->json($data);
    }
    
}
