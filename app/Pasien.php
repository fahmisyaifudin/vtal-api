<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pasien extends Model
{
    protected $table = 'pasien';
    protected $fillable = ['name', 'email', 'password', 'nik', 'gender', 'address', 'birth_place', 'birth_date', 'phone', 'image', 'status'];
    protected $hidden = ['created_at','updated_at'];
    public $incrementing = false;
}
