<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class partner extends Model
{
    protected $table = 'partner';
    protected $fillable = ['name', 'email', 'partner_type', 'password', 'nik', 'gender', 'address', 'birth_place', 'birth_date', 'phone', 'image', 'status'];
    protected $hidden = ['created_at','updated_at'];
    public $incrementing = false;
}
