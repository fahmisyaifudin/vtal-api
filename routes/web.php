<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});



//Pasien
$router->post('/pasien/register', 'PasienAuthController@register');
$router->post('/pasien/login', 'PasienAuthController@login');

// $router->post('/pasien/show', 'PasienAuthController@show');

$router->post('/pasien/show', ['middleware' => 'token', 
                                'uses'=>'PasienAuthController@show']);


$router->group(['middleware' => 'token'], function () use ($router) {
    $router->post('/pasien/show', 'PasienAuthController@show');
});

//Partner  
$router->post('/partner/register', 'PartnerAuthController@register'); 
$router->group(['middleware' => 'token'], function () use ($router) {
    $router->post('/partner/show', 'PartnerAuthController@show');
});                             